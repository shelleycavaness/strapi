import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class AuthorService {
  apiURL = "http://localhost:1337/";

  constructor(private http: HttpClient) {}

  getAuthors() {
    return this.http.get(`${this.apiURL}authors`);
  }
  getBookByAuthor(name) {
    return this.http.get(`${this.apiURL}books?author.name=${name}`);
  }
}
