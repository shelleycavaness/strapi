import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { catchError, mapTo, tap } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class UserService {
  apiURL = "http://localhost:1337/";

  constructor(private http: HttpClient) {}

  connect(connectUser) {
    return this.http.post(`${this.apiURL}auth/local`, connectUser).pipe(
      tap((user) => this.doLogin(user)),
      mapTo(true),
      catchError((err) => err)
    );
  }
  doLogin(user: any) {
    localStorage.setItem("currentUser", JSON.stringify(user));
  }
  updateUser(body) {
    return this.http.put(`${this.apiURL}users/${body.id}`, body);
  }
  getMy(id) {
    return this.http.get(`${this.apiURL}users/${id}`);
  }
}
