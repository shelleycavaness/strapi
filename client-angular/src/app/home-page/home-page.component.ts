import { Component, OnInit } from "@angular/core";
import { BooksService } from "../services/books.service";

@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
  styleUrls: ["./home-page.component.scss"],
})
export class HomePageComponent implements OnInit {
  books: any;
  constructor(private bookService: BooksService) {}

  ngOnInit() {
    this.getBooks();
  }

  getBooks() {
    this.bookService.getBooks().subscribe((res) => {
      this.books = res;
      console.dir(this.books);
    });
  }
}
