import { Component, OnInit } from "@angular/core";
import {
  Validators,
  FormBuilder,
  FormGroup,
  FormControl,
} from "@angular/forms";
import { UserService } from "src/app/services/user.service";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-connect-modal",
  templateUrl: "./connect-modal.component.html",
  styleUrls: ["./connect-modal.component.scss"],
})
export class ConnectModalComponent implements OnInit {
  currentUser;

  FormConnection: FormGroup = this.fb.group({
    identifier: ["", [Validators.required]],
    password: ["", [Validators.required, Validators.minLength(6)]],
  });

  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private userService: UserService
  ) {}

  ngOnInit() {}

  connect() {
    this.userService.connect(this.FormConnection.value).subscribe(
      (data) => {
        this.getCurrentUser();
        this.closeModal(this.currentUser);
      },
      (err) => {
        console.dir(err);
      }
    );
  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return JSON.parse(localStorage.getItem("currentUser"));
  }

  closeModal(currentUser) {
    this.activeModal.close(currentUser);
  }
}
