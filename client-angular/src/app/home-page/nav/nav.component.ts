import { Component, OnInit } from "@angular/core";
import { NgbModal, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ConnectModalComponent } from "../connect-modal/connect-modal.component";

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.scss"],
})
export class NavComponent implements OnInit {
  currentUser: any;
  constructor(private modalService: NgbModal) {}

  ngOnInit() {
    this.getCurrentUser();
  }
  openModalConnect() {
    const modalRef = this.modalService.open(ConnectModalComponent);
    modalRef.result.then((result) => {
      this.currentUser = result;
      console.dir(result);
    });
  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    console.dir(this.currentUser);
  }

  canActivate() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    if (this.currentUser) {
      return true;
    } else {
      return false;
    }
  }
}
