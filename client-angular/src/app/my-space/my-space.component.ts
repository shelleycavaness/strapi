import { Component, OnInit } from "@angular/core";
import { UserService } from "../services/user.service";
import { BooksService } from "../services/books.service";

@Component({
  selector: "app-my-space",
  templateUrl: "./my-space.component.html",
  styleUrls: ["./my-space.component.scss"],
})
export class MySpaceComponent implements OnInit {
  currentUser: any;
  currentUserId;
  myBooks;
  sortedDate: boolean = true;
  nbCurrentBook;
  nbNewBook;
  constructor(
    private bookService: BooksService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.getCurrentUser();
    this.getMyBooks();
  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.currentUserId = this.currentUser.user.id;
    // console.dir(this.currentUser);
  }
  getMyBooks() {
    this.bookService.getMyBooks(this.currentUserId).subscribe((res) => {
      this.myBooks = res;
      // console.dir(this.myBooks);
    });
  }
  sortByDateAsc() {
    this.sortedDate = false;
    this.bookService.sortMyBookAsc(this.currentUserId).subscribe((res) => {
      this.myBooks = res;
      // console.dir(this.myBooks);
    });
  }

  sortByDateDesc() {
    this.sortedDate = true;
    this.bookService.sortMyBookDesc(this.currentUserId).subscribe((res) => {
      this.myBooks = res;
      // console.dir(this.myBooks);
    });
  }

  deleteMyBook(id, bookId) {
    this.bookService.deleteMyBook(id).subscribe((res) => {
      // console.dir(res);
      this.getMyBooks();
      this.nbBookBorrow();
      let bodyBook = {
        available: true,
        id: bookId,
      };
      this.updateBook(bodyBook);
    });
  }

  updateBook(body) {
    this.bookService.updateBook(body).subscribe((res) => {
      console.dir(res);
    });
  }
  nbBookBorrow() {
    this.userService.getMy(this.currentUserId).subscribe((res: any) => {
      this.nbCurrentBook = res.nb_books_borrowed;
      // console.log(this.nbCurrentBook);
      this.nbNewBook = this.nbCurrentBook -= 1;
      this.currentUser.user.nb_books_borrowed = this.nbNewBook;
      this.updateUser(this.nbNewBook, this.currentUserId);
    });
  }
  updateUser(nbNewBook, currentUserId) {
    let body = {
      id: currentUserId,
      nb_books_borrowed: nbNewBook,
    };
    this.userService.updateUser(body).subscribe((res: any) => {
      this.nbCurrentBook = res.nb_books_borrowed;
      // console.log(this.nbCurrentBook);
    });
  }
}
